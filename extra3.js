/*Escolha 5 valores inteiros e ordene-os em ordem crescente. No final,
mostre os valores em ordem crescente, uma linha em branco e em
seguida, os valores na sequência*/

let valores = []
for (let i = 0; i < 5; i++){
    let valor = prompt('Diga os valores:');
    valores.push(valor)
}  

valores.sort((a,b) => a - b);
console.log('Valores ordenados:', valores);
console.log('');
console.log(valores.join(''));
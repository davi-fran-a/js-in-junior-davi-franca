var Linhas1 = prompt('Numero de linhas da matriz 1?');
var Colunas1 = prompt('Numero de colunas da matriz 1?');
var Linhas2 = prompt('Numero de colunas da matriz 2?');
var Colunas2 = prompt('Numero de colunas da matriz 2?');

if (Colunas1 !== Linhas2) {
  console.log('Não dá para multiplicar essas matrizes!')
 }
else {
  var mat1 = [];
  var mat2 = [];
  var res = [];
  for (var i = 0; i < Linhas1; i++) {
    res[i] = [];
    mat1[i] = [];
    for (var j = 0; j < Colunas1; j++) {
      mat1[i][j] = parseFloat(prompt('Digite os valores da matriz:'));
    }
    for(var k = 0; k < Colunas2; k++) {
      mat2[k] = mat2[k] || [];
      res[i][k] = 0;
      for(var j = 0; j < Colunas1; j++) {
        mat2[k][j] = mat2[k][j] || 0;
        mat2[k][j] += parseFloat(prompt('Digite os valores da matriz:'));
        res[i][k] += mat1[i][j]*mat2[k][j];
      }
    }
  }
  for (var i = 0; i < Linhas1; i++) {
    console.log(res[i]);
  }
}